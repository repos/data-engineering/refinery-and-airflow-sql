-- Extracts data from the gdi.grants table and stores the transformations
--
-- Parameters:
--     source_table         -- Read raw data from here
--     world_bank_data      -- Read data from world_bank from here
--     destination_table    -- Insert results here
--     year                 -- YYYY to compute statistics for
--
-- Usage:
--     hive -f grants_leadership_input_metrics.hql                    \
--          -d source_table=gdi.grants_input_metrics                  \
--          -d destination_table=gdi.grants_leadership_input_metrics  \
--          -d year=2021
--
WITH grant_totals_by_year_and_country AS (
    SELECT
        country_code,
        ROUND(SUM(CAST(COALESCE(usd_over_grant_life, 0) AS double)), 2) AS total_grants_calendar_year,
        COUNT(*) AS count_grants_calendar_year,
        CAST(calendar_year AS int) AS calendar_year
    FROM ${source_table}
    WHERE
        year = ${year}
        AND CAST(calendar_year AS int) <= ${year}
    GROUP BY country_code, calendar_year
),

total_historical_grants_by_country AS (
    SELECT
        country_code,
        SUM(total_grants_calendar_year) AS total_historical_grants,
        COUNT(count_grants_calendar_year) AS historical_grant_count
    FROM grant_totals_by_year_and_country
    GROUP BY country_code
),

historical_grants_weighted AS (
    SELECT
        country_code,
        SUM(dollar_weighted_constant) AS total_historical_grants_weighted,
        SUM(IF(calendar_year = ${year}, dollar_weighted_current, 0)) AS total_ann_grants_weighted
    FROM ${source_table}
    WHERE
        year = ${year}
        AND calendar_year <= ${year}
    GROUP BY country_code
),

affiliate_led_grant_totals_by_year_and_country AS (
    SELECT
        country_code,
        ROUND(SUM(CAST(COALESCE(usd_over_grant_life, 0) AS double)), 2) AS total_calendar_year_grants,
        COUNT(*) AS count_affiliate_led_grants_cal_year,
        CAST(calendar_year AS int) AS calendar_year
    FROM ${source_table}
    WHERE
        year = ${year}
        AND LOWER(org_type) IN ('chapter', 'thematic organization', 'user group')
    GROUP BY country_code, calendar_year
),

total_historical_affiliate_led_grants_by_country AS (
    SELECT
        country_code,
        SUM(total_calendar_year_grants) AS total_historical_grants_to_date,
        COUNT(count_affiliate_led_grants_cal_year) AS count_affiliate_led_grants_hist
    FROM affiliate_led_grant_totals_by_year_and_country
    WHERE calendar_year <= ${year}
    GROUP BY country_code
),

historical_affiliate_grants_weighted AS (
    SELECT
        country_code,
        SUM(dollar_weighted_constant) AS total_historical_affiliate_grants_weighted,
        SUM(IF(calendar_year = ${year}, dollar_weighted_current, 0)) AS total_ann_affiliate_grants_weighted
    FROM ${source_table}
    WHERE
        year = ${year}
        AND calendar_year <= ${year}
        AND LOWER(org_type) IN ('chapter', 'thematic organization', 'user group')
    GROUP BY country_code
),

average_grant_totals_last_5_years_by_country AS (
    SELECT
        country_code,
        AVG(total_grants_calendar_year) AS average_grants
    FROM grant_totals_by_year_and_country
    WHERE calendar_year BETWEEN ${year} - 4 AND ${year}
    GROUP BY country_code
),

average_grant_totals_previous_5_years_by_country AS (

    SELECT
        country_code,
        AVG(total_grants_calendar_year) AS average_grants
    FROM grant_totals_by_year_and_country
    WHERE calendar_year BETWEEN ${year} - 9 AND ${year} - 5
    GROUP BY country_code
)
,

grant_loss_comparing_last_5_years_to_previous_5_years AS (

    SELECT
        country.country_code,
        grant_5.average_grants AS average_grants_5,
        grant_10.average_grants AS average_grants_10,
        grant_10.average_grants / grant_5.average_grants AS avg_ann_5_yr_change
    FROM total_historical_grants_by_country AS country
    LEFT JOIN average_grant_totals_last_5_years_by_country AS grant_10 ON (country.country_code = grant_10.country_code)
    LEFT JOIN
        average_grant_totals_previous_5_years_by_country AS grant_5
        ON (country.country_Code = grant_5.country_code)

),

five_year_grant_loss_or_gain_by_country AS (

    SELECT
        country_code,
        average_grants_5,
        average_grants_10,
        ROUND(avg_ann_5_yr_change, 2) AS average_change_5,
        ROUND(-1 + avg_ann_5_yr_change, 2) AS loss_or_gain
    FROM grant_loss_comparing_last_5_years_to_previous_5_years
),

grants_data AS (
    SELECT
        hist.total_historical_grants AS historical_grants_to_date,
        hist.country_code,
        total_grants.total_grants_calendar_year,
        hist.historical_grant_count,
        total_grants.count_grants_calendar_year,
        hist_w.total_historical_grants_weighted,
        hist_w.total_ann_grants_weighted
    FROM total_historical_grants_by_country AS hist
    LEFT JOIN
        grant_totals_by_year_and_country AS total_grants
        ON (hist.country_code = total_grants.country_code AND total_grants.calendar_year = ${year})
    LEFT JOIN historical_grants_weighted AS hist_w ON (hist.country_code = hist_w.country_code)
),

final_grants_data AS (
    SELECT
        gdata.country_code,
        gdata.total_grants_calendar_year,
        gdata.historical_grants_to_date,
        gdata.historical_grant_count,
        gdata.count_grants_calendar_year,
        gdata.total_historical_grants_weighted,
        gdata.total_ann_grants_weighted,
        1 + (COALESCE(lorg.loss_or_gain, 0) / 5) AS loss_or_gain_five_year
    FROM grants_data AS gdata
    LEFT JOIN five_year_grant_loss_or_gain_by_country AS lorg ON (gdata.country_code = lorg.country_code)
)

INSERT OVERWRITE TABLE ${destination_table} PARTITION (year = '${year}')
SELECT *
FROM (
    SELECT
        fg_data.country_code,
        COALESCE(fg_data.historical_grants_to_date, 0) AS total_historical_grants_to_date,
        COALESCE(fg_data.total_grants_calendar_year, 0) AS total_calendar_year_grants,
        COALESCE(fg_data.historical_grant_count, 0) AS count_historical_grants_to_date,
        COALESCE(fg_data.count_grants_calendar_year, 0) AS count_calendar_year_grants,
        COALESCE(fg_data.total_ann_grants_weighted, 0) AS total_annual_grants_presence_weighted,
        COALESCE(fg_data.total_historical_grants_weighted, 0) AS total_historical_grants_presence_weighted,
        COALESCE(fg_data.loss_or_gain_five_year, 0) AS five_year_grants_change,
        COALESCE(fg_data.total_ann_grants_weighted * loss_or_gain_five_year, 0) AS total_ann_grants_five_year_change,
        COALESCE(
            fg_data.total_historical_grants_weighted * loss_or_gain_five_year, 0
        ) AS total_historical_grants_five_year_change,
        COALESCE(cal.total_calendar_year_grants, 0) AS total_annual_grants_annual_signal,
        COALESCE(hist.total_historical_grants_to_date, 0) AS total_historical_grants_annual_signal,
        COALESCE(cal.count_affiliate_led_grants_cal_year, 0) AS count_affiliate_led_grants_annual_signal,
        COALESCE(hist.count_affiliate_led_grants_hist, 0) AS count_historical_affiliate_led_grants_annual_signal,
        COALESCE(hist_w.total_ann_affiliate_grants_weighted, 0) AS total_annual_affiliate_led_grants_presence_weighted,
        COALESCE(
            hist_w.total_historical_affiliate_grants_weighted, 0
        ) AS total_historical_affiliate_led_grants_presence_weighted
    FROM final_grants_data AS fg_data
    LEFT JOIN
        affiliate_led_grant_totals_by_year_and_country AS cal
        ON (fg_data.country_code = cal.country_code AND cal.calendar_year = ${year})
    LEFT JOIN total_historical_affiliate_led_grants_by_country AS hist ON (fg_data.country_code = hist.country_code)
    LEFT JOIN historical_affiliate_grants_weighted AS hist_w ON (fg_data.country_code = hist_w.country_code)
) AS `data`
WHERE data.country_code IS NOT NULL
