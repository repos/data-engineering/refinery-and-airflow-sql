-- Aggregates data from geoeditors_monthly table to create active editors metrics
--
-- Parameters:
--     source_table         -- Read raw data from here
--     geoeditor_metrics    -- Read geoeditor_input_metrics from here
--     destination_table    -- Insert results here
--     year                 -- YYYY to compute statistics for
--
-- Usage:
--     hive -f geoeditor_online_input_metrics.hql                         \
--          -d source_table=wmf.geoeditors_monthly                        \
--          -d country_data=gdi.country_meta_data                         \
--          -d destination_table=gdi.geoeditor_online_input_metrics       \
--          -d year=2021
--
WITH country_data AS (
    SELECT DISTINCT
        country_code_iso_3,
        country_code_iso_2,
        FIRST_VALUE(canonical_country_name) OVER (PARTITION BY country_code_iso_3) AS country_name
    FROM ${country_data}
    WHERE country_code_iso_3 IS NOT NULL
),

average_active_editors AS (
    SELECT
        country_code,
        SUM(distinct_editors) / COUNT(DISTINCT month) AS monthly_distinct_editors
    FROM ${source_table}
    WHERE
        month LIKE '${year}-%'
        AND activity_level != '1 to 4'
    GROUP BY country_code
),

monthly_edits AS (
    SELECT
        data.country_code,
        AVG(data.distinct_editors) AS monthly_avg_editors
    FROM (
        SELECT
            country_code,
            month,
            SUM(distinct_editors) AS distinct_editors
        FROM ${source_table}
        WHERE month LIKE '${year}-%'
        GROUP BY
            country_code,
            month
    ) AS `data`
    GROUP BY data.country_code
)

INSERT OVERWRITE TABLE ${destination_table} PARTITION (year = '${year}')
SELECT
    country.country_code_iso_2,
    act.monthly_distinct_editors,
    monthly.monthly_avg_editors,
    IF(
        monthly.monthly_avg_editors = 0, 0, act.monthly_distinct_editors / monthly.monthly_avg_editors
    ) AS percent_editors_active
FROM country_data AS country
LEFT JOIN average_active_editors AS act ON (country.country_code_iso_2 = act.country_code)
LEFT JOIN monthly_edits AS monthly ON (country.country_code_iso_2 = monthly.country_code);
