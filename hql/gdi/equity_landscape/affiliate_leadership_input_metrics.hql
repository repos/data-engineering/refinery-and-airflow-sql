-- Stores affiliate leadership data based on multiple gdi sources.
--
-- Parameters:
--     affiliate_data       -- Read affiliate data from here
--     country_data         -- Read country data from here
--     grants_leadership    -- Read grants leadership data from here
--     destination_table    -- Insert results here
--     year                 -- YYYY to compute statistics for
--
-- Usage:
--     hive -f affiliate_leadership_input_metrics.hql                         \
--          -d affiliate_data=gdi.affiliate_data_input_metrics                \
--          -d country_data=gdi.country_meta_data                             \
--          -d grants_leadership=gdi.grants_leadership_input_metrics          \
--          -d destination_table=gdi.affiliate_leadership_input_metrics       \
--          -d year=2021
--
WITH country_data AS (
    SELECT DISTINCT
        iso3_country_code,
        iso2_country_code,
        FIRST_VALUE(country_area_label) OVER (PARTITION BY iso3_country_code) AS country_area_label
    FROM ${country_data}
    WHERE iso3_country_code IS NOT NULL
),

csv_affiliates AS (
    SELECT
        country_code,
        COUNT(*) AS count_affiliates_in_country,
        MAX(affiliate_size) AS affiliate_max_size,
        DATEDIFF(TO_DATE('${year}-12-31'), MIN(start_date)) / 365 AS total_affiliate_tenure,
        MAX(size_growth) AS size_growth,
        MAX(governance_type) AS governance_type
    FROM ${affiliate_data}
    WHERE year = ${year}
    GROUP BY country_code

)

INSERT OVERWRITE TABLE ${destination_table} PARTITION (year = '${year}')
SELECT
    metr.country_code,
    metr.count_affiliates_in_country,
    metr.affiliate_max_size,
    metr.size_growth,
    metr.total_affiliate_tenure,
    grants_lead.total_calendar_year_grants_lead,
    grants_lead.total_calendar_year_grants_lead / grants_lead.total_calendar_year_grants_lead_affiliates AS percent_annual_grants_lead_affiliate,
    grants_lead.total_historical_grants_lead_to_date
    / grants_lead.total_historical_grants_lead_affiliates AS percent_historical_grants_lead_affiliate,
    grants_lead.total_historical_grants_lead_affiliates,
    grants_lead.count_calendar_year_grants_lead,
    grants_lead.count_historical_grants_lead_to_date,
    metr.governance_type
FROM csv_affiliates AS metr
LEFT JOIN ${grants_lead_leadership} AS grants_lead ON (metr.country_code = grants_lead.country_code AND grants_lead.year = ${year});
