-- Stores various metrics from gdi sources as population data.
--
-- Parameters:
--     source_table         -- Read raw data from here
--     metrics_table        -- Read metrics data from here
--     country_data         -- Read country data from here
--     pivot_table          -- Read pivot data from here
--     destination_table    -- Insert results here
--     year                 -- YYYY to compute statistics for
--
-- Usage:
--     hive -f population_leadership_input_metrics.hql                    \
--          -d source_table=gdi.population_data_input_metrics             \
--          -d metrics_table=gdi.geoeditor_online_input_metrics           \
--          -d country_data=gdi.country_meta_data                         \
--          -d pivot_table=gdi.geoeditor_input_metrics_pivot              \
--          -d destination_table=gdi.population_leadership_input_metrics  \
--          -d year=2021
--


WITH country_data AS (
    SELECT DISTINCT
        country_code_iso_3,
        country_code_iso_2,
        FIRST_VALUE(canonical_country_name) OVER (PARTITION BY country_code_iso_3) AS country_name
    FROM ${country_data}
    WHERE country_code_iso_3 IS NOT NULL
),

wikipedia_editors AS (
    SELECT
        country_code,
        wikipedia
    FROM ${pivot_table}
    WHERE
        metric = 'monthly_bins'
        AND year = ${year}
),

average_editors AS (
    SELECT
        country_code,
        monthly_edits_avg,
        monthly_distinct_editors
    FROM ${metrics_table}
    WHERE year = ${year}
)

INSERT OVERWRITE TABLE ${destination_table} PARTITION (year = '${year}')
SELECT
    country.country_code_iso_3,
    COALESCE(population.population_annual_signal, 0) AS population_annual_signal,
    COALESCE(population.population_annual_change, 0) AS population_annual_change,
    COALESCE(wedit.wikipedia, 0) AS wikipedia_annual_signal,
    COALESCE(avg_cal_year.monthly_edits_avg, COALESCE(wedit.wikipedia, 0)) AS monthly_edits_avg,
    COALESCE(avg_cal_year.monthly_distinct_editors, 0) AS monthly_distinct_editors,
    COALESCE(population.gdp_per_capita_ppp_current, 0) AS gdp_per_capita_ppp_current,
    COALESCE(population.gdp_per_capita_ppp_constant, 0) AS gdp_per_capita_ppp_constant,
    COALESCE(population.internet_percent_annual_signal, 0) AS internet_percent_annual_signal
FROM country_data AS country
LEFT JOIN ${source_table} AS population ON (country.country_code_iso_3 = population.country_code AND year = ${year} - 1)
LEFT JOIN wikipedia_editors AS wedit ON (country.country_code_iso_2 = wedit.country_code)
LEFT JOIN average_editors AS avg_cal_year ON (country.country_code_iso_2 = avg_cal_year.country_code);
