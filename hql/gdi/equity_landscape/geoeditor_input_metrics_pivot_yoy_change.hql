-- Calculates the year-on-year change for the previous year and the current year from the geoeditor_metric_pivot table per country
--
-- Parameters:
--     source_table         -- Read raw data from here
--     destination_table    -- Insert results here
--     year                 -- YYYY to compute statistics for
--     metric               -- Metric to compute statistics for
--
-- Usage:
--     hive -f geoeditor_input_metrics_pivot_yoy_change.hql             \
--          -d source_table=gdi.geoeditor_input_metrics_pivot           \
--          -d destination_table=gdi.geoeditor_input_metrics_pivot      \
--          -d year=2021
--          -d metric=yoy_change
--

DROP TABLE IF EXISTS ${source_table}_temp;

CREATE TABLE IF NOT EXISTS ${source_table}_temp AS -- This done to prevent the cannot write from a source that's being read.
SELECT *
FROM ${source_table}
WHERE
    year IN (${year} - 1, ${year})
    AND metric = 'monthly_bins';

INSERT OVERWRITE TABLE ${destination_table} PARTITION (year = '${year}', metric = '${metric}')
SELECT
    country_code,
    commons,
    mediawiki,
    wikidata,
    wikipedia,
    wikisource,
    sister_project,
    organizing_wiki
FROM (
    SELECT
        country_code,
        year,
        commons / LAG(commons) OVER (PARTITION BY country_code ORDER BY year) AS commons,
        mediawiki / LAG(mediawiki) OVER (PARTITION BY country_code ORDER BY year) AS mediawiki,
        wikidata / LAG(wikidata) OVER (PARTITION BY country_code ORDER BY year) AS wikidata,
        wikipedia / LAG(wikipedia) OVER (PARTITION BY country_code ORDER BY year) AS wikipedia,
        wikisource / LAG(wikisource) OVER (PARTITION BY country_code ORDER BY year) AS wikisource,
        sister_project / LAG(sister_project) OVER (PARTITION BY country_code ORDER BY year) AS sister_project,
        organizing_wiki / LAG(organizing_wiki) OVER (PARTITION BY country_code ORDER BY year) AS organizing_wiki
    FROM ${source_table}_temp
    WHERE
        year IN (${year} - 1, ${year})
        AND metric = 'monthly_bins'
) AS `data`
WHERE year = ${year};

DROP TABLE ${source_table}_temp;
