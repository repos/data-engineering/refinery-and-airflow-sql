CREATE EXTERNAL TABLE IF NOT EXISTS gdi.wiki_db_map_input_metrics (
    database_code string COMMENT 'database code',
    database_group string COMMENT 'database group',
    grouped_bin string COMMENT 'grouped bin name',
    language_code string COMMENT 'language code',
    language_name string COMMENT 'language name'
)
COMMENT 'Table to store the wiki project mapping to project database'
STORED AS PARQUET
    LOCATION
    'hdfs://analytics-hadoop/wmf/data/gdi/wiki_db_map_input_metrics';
