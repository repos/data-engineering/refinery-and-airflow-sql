-- Extracts one day of formatted minutely banner activity to be loaded in Druid
--
-- Usage:
--     spark-sql -f generate_daily_druid_banner_activity.hql \
--         -d source_table=wmf.webrequest \
--         -d destination_table=tmp_banner_activity_2023_01_01 \
--         -d destination_directory=/wmf/tmp/druid/daily_banner_activity \
--         -d coalesce_partitions=1 \
--         -d year=2023 \
--         -d month=1 \
--         -d day=1


DROP TABLE IF EXISTS ${destination_table};
CREATE TABLE IF NOT EXISTS ${destination_table} (
    `dt` string,
    `campaign` string,
    `banner` string,
    `project` string,
    `uselang` string,
    `bucket` string,
    `anonymous` boolean,
    `status_code` string,
    `country` string,
    `country_matches_geocode` boolean,
    `region` string,
    `device` string,
    `sample_rate` float,
    `request_count` bigint,
    `normalized_request_count` bigint
)
USING PARQUET
OPTIONS ('compression' = 'gzip')
    LOCATION '${destination_directory}';


WITH filtered_data AS (
    SELECT
        geocoded_data,
        CONCAT(SUBSTRING(dt, 0, 17), '00Z') AS dt,
        CONCAT('http://bla.org/woo/', uri_query) AS url
    FROM
        ${source_table}
    WHERE
        year = ${year}
        AND month = ${month}
        AND day = ${day}
        AND webrequest_source = 'text'
        -- drop requests with no timestamps
        AND dt != '-'
        AND uri_path = '/beacon/impression'
        AND agent_type = 'user'
        -- TODO: add once added to webrequest
        -- AND x_analytics_map['proxy'] IS NULL
)


INSERT OVERWRITE TABLE ${destination_table}
SELECT /*+ COALESCE(${coalesce_partitions}) */
    dt,
    PARSE_URL(url, 'QUERY', 'campaign') AS campaign,
    PARSE_URL(url, 'QUERY', 'banner') AS banner,
    PARSE_URL(url, 'QUERY', 'project') AS project,
    PARSE_URL(url, 'QUERY', 'uselang') AS uselang,
    PARSE_URL(url, 'QUERY', 'bucket') AS `bucket`,
    PARSE_URL(url, 'QUERY', 'anonymous') = 'true' AS anonymous,
    PARSE_URL(url, 'QUERY', 'statusCode') AS status_code,
    PARSE_URL(url, 'QUERY', 'country') AS country,
    geocoded_data['country_code'] = PARSE_URL(url, 'QUERY', 'country') AS country_matches_geocode,
    geocoded_data['subdivision'] AS region,
    PARSE_URL(url, 'QUERY', 'device') AS device,
    CAST(PARSE_URL(url, 'QUERY', 'recordImpressionSampleRate') AS float) AS sample_rate,
    COUNT(*) AS request_count,
    CAST(
        COUNT(*) / CAST(PARSE_URL(url, 'QUERY', 'recordImpressionSampleRate') AS float) AS bigint
    ) AS normalized_request_count
FROM
    filtered_data
WHERE
    PARSE_URL(url, 'QUERY', 'debug') = 'false'
    -- sample_rate can be infinity, leading to Druid indexation failing.
    -- We remove those rows from the data
    AND CAST(PARSE_URL(url, 'QUERY', 'recordImpressionSampleRate') AS float) != 'Infinity'
GROUP BY
    dt,
    PARSE_URL(url, 'QUERY', 'campaign'),
    PARSE_URL(url, 'QUERY', 'banner'),
    PARSE_URL(url, 'QUERY', 'project'),
    PARSE_URL(url, 'QUERY', 'uselang'),
    PARSE_URL(url, 'QUERY', 'bucket'),
    PARSE_URL(url, 'QUERY', 'anonymous') = 'true',
    PARSE_URL(url, 'QUERY', 'statusCode'),
    PARSE_URL(url, 'QUERY', 'country'),
    geocoded_data['country_code'] = PARSE_URL(url, 'QUERY', 'country'),
    geocoded_data['subdivision'],
    PARSE_URL(url, 'QUERY', 'device'),
    CAST(PARSE_URL(url, 'QUERY', 'recordImpressionSampleRate') AS float);
