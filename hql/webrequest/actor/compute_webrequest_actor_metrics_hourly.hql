-- Computing hourly metrics per webrequest actor to use them
-- rolled-up to label automated traffic
-- Companion doc: https://docs.google.com/document/d/1q14GH7LklhMvDh0jwGaFD4eXvtQ5tLDmw3UeFTmb3KM/edit

-- Parameters:
--     refinery_hive_jar_path -- The path to the refinery-hive jar to use for UDFs
--     version                -- The version of the metrics we gather
--     source_table           -- Fully qualified table name to compute the
--                               aggregation from.
--     destination_table      -- Fully qualified table name to fill in
--                               aggregated values.
--     year                   -- year of partition to compute aggregation for.
--     month                  -- month of partition to compute aggregation for.
--     day                    -- day of partition to compute aggregation for.
--     hour                   -- hour of partition to compute aggregation for.
--     coalesce_partitions    -- The number of files to write per hour
--
-- Usage:
--     spark3-sql -f compute_webrequest_actor_metrics_hourly.hql     \
--         -d refinery_hive_jar_path=hdfs:///wmf/refinery/current/artifacts/refinery-hive-shaded.jar \
--         -d source_table=wmf.webrequest                            \
--         -d destination_table=wmf.webrequest_actor_metrics_hourly  \
--         -d version=0.1                                            \
--         -d year=2023                                              \
--         -d month=2                                                \
--         -d day=1                                                  \
--         -d hour=1                                                 \
--         -d coalesce_partitions=2
--

ADD JAR ${refinery_hive_jar_path};
CREATE TEMPORARY FUNCTION GET_ACTOR_SIGNATURE AS 'org.wikimedia.analytics.refinery.hive.GetActorSignatureUDF';

WITH hourly_actor_data AS (
    SELECT
        ts,
        ip,
        http_status,
        user_agent,
        LOWER(uri_host) AS `domain`,
        GET_ACTOR_SIGNATURE(ip, user_agent, accept_language, uri_host, uri_query, x_analytics_map) AS actor_signature,
        x_analytics_map['nocookies'] AS nocookies,
        pageview_info['page_title'] AS page_title
    FROM
        ${source_table}
    WHERE
        webrequest_source = 'text'
        AND year = ${year}
        AND month = ${month}
        AND day = ${day}
        AND hour = ${hour}
        AND is_pageview = 1
        AND agent_type = 'user'
        -- weblight data is a mess, there is no x-forwarded-for and all looks like it comes from the same IP
        AND user_agent NOT LIKE '%weblight%'
        AND COALESCE(pageview_info['project'], '') != ''
)


INSERT OVERWRITE TABLE ${destination_table}
PARTITION (year = ${year}, month = ${month}, day = ${day}, hour = ${hour})

SELECT /*+ COALESCE(${coalesce_partitions}) */
    ${version},
    actor_signature,
    MIN(ts) AS first_interaction_dt,
    MAX(ts) AS last_interaction_dt,
    COUNT(*) AS pageview_count,
    CAST((COUNT(*) / (UNIX_TIMESTAMP(MAX(ts)) - UNIX_TIMESTAMP(MIN(ts))) * 60) AS int) AS pageview_rate_per_min,
    SUM(COALESCE(nocookies, 0L)) AS nocookies,
    MAX(LENGTH(user_agent)) AS user_agent_length,
    COUNT(DISTINCT page_title) AS distinct_pages_visited_count
FROM
    hourly_actor_data
GROUP BY
    actor_signature;
