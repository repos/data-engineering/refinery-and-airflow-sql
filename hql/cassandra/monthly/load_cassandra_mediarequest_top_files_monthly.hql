-- Loads the mediarequest top_files monthly dataset to cassandra
-- Parameters:
--     destination_table     -- Cassandra table to write query output.
--     source_table          -- Fully qualified hive table to compute from.
--     year                   -- year of partition to compute from.
--     month                  -- month of partition to compute from.
--     coalesce_partitions    -- number of partitions for destination data.

-- Usage:
-- spark-sql \
-- --driver-cores 1 \
-- --master yarn \
-- --conf spark.sql.catalog.aqs=com.datastax.spark.connector.datasource.CassandraCatalog \
-- --conf spark.sql.catalog.aqs.spark.cassandra.connection.host=aqs1010-a.eqiad.wmnet:9042,aqs1011-a.eqiad.wmnet:9042,aqs1012-a.eqiad.wmnet:9042 \
-- --conf spark.sql.catalog.aqs.spark.cassandra.auth.username=aqsloader \
-- --conf spark.sql.catalog.aqs.spark.cassandra.auth.password=cassandra \
-- --conf spark.sql.catalog.aqs.spark.cassandra.output.batch.size.rows=1024 \
-- --jars /srv/deployment/analytics/refinery/artifacts/org/wikimedia/analytics/refinery/refinery-job-0.2.4-shaded.jar \
-- --conf spark.dynamicAllocation.maxExecutors=128 \
-- --conf spark.yarn.maxAppAttempts=1 \
-- --conf spark.executor.memoryOverhead=3072  \
-- --executor-memory 8G \
-- --executor-cores 2 \
-- --driver-memory 4G \
-- --name mediarequest_top_files_per_month \
--     -f load_cassandra_mediarequest_top_files_monthly.hql \
--     -d destination_table=aqsl.local_group_default_T_mediarequest_top_files.data \
--     -d source_table=wmf.mediarequest \
--     -d coalesce_partitions=6 \
--     -d year=2022 \
--     -d month=07

WITH ranked AS (
    SELECT
        referer,
        file_path,
        media_classification,
        year,
        month,
        requests,
        RANK() OVER (PARTITION BY referer, media_classification, year, month ORDER BY requests DESC) AS `rank`,
        ROW_NUMBER() OVER (PARTITION BY referer, media_classification, year, month ORDER BY requests DESC) AS rn
    FROM (
        SELECT
            COALESCE(IF(referer = 'external (search engine)', 'search-engine', referer), 'all-referers') AS referer,
            REFLECT('org.json.simple.JSONObject', 'escape', REGEXP_REPLACE(base_name, '\t', '')) AS file_path,
            COALESCE(media_classification, 'all-media-types') AS media_classification,
            LPAD(year, 4, '0') AS `year`,
            LPAD(month, 2, '0') AS `month`,
            SUM(request_count) AS requests
        FROM ${source_table}
        WHERE
            year = ${year}
            AND month = ${month}
            AND agent_type = 'user'
        GROUP BY
            referer, REGEXP_REPLACE(base_name, '\t', ''), media_classification, year, month
            GROUPING SETS (
                (
                    year,
                    month,
                    referer,
                    REGEXP_REPLACE(base_name, '\t', ''),
                    media_classification
                ), (
                    year,
                    month,
                    REGEXP_REPLACE(base_name, '\t', ''),
                    media_classification
                ), (
                    year,
                    month,
                    referer,
                    REGEXP_REPLACE(base_name, '\t', '')
                ), (
                    year,
                    month,
                    REGEXP_REPLACE(base_name, '\t', '')
                )
            )
    ) AS `raw`
),

max_rank AS (
    SELECT
        referer AS max_rank_referer,
        media_classification AS max_rank_media_classification,
        year AS max_rank_year,
        month AS max_rank_month,
        rank AS max_rank
    FROM ranked
    WHERE
        rn = 1001
    GROUP BY
        referer,
        media_classification,
        year,
        month,
        rank
)

INSERT INTO ${destination_table}
SELECT
/*+ COALESCE(${coalesce_partitions}) */
    'analytics.wikimedia.org' AS _domain,
    r.referer,
    r.media_classification AS media_type,
    r.year,
    r.month,
    'all-days' AS `day`,
    '13814000-1dd2-11b2-8080-808080808080' AS _tid,
    CONCAT(
        '[',
        CONCAT_WS(',', SORT_ARRAY(
            COLLECT_SET(
                CONCAT(
                    '{"file_path":"', r.file_path,
                    '","requests":', CAST(r.requests AS STRING),
                    ',"rank":', CAST(r.rank AS STRING), '}'
                )
            )
        )), ']'
    ) AS filesjson
FROM ranked AS r
LEFT JOIN max_rank AS mr
    ON (
        r.referer = mr.max_rank_referer
        AND r.media_classification = mr.max_rank_media_classification
        AND r.year = mr.max_rank_year
        AND r.month = mr.max_rank_month
    )
WHERE r.rank < COALESCE(mr.max_rank, 1001)
GROUP BY
    r.referer,
    r.media_classification,
    r.year,
    r.month
