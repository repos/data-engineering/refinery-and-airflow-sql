-- Load knowledge gaps content gap metrics into cassandra
-- Parameters:
--      destination table: The cassandra table being written to
--      source_table: the content_gap_metrics table in hive
--      date: monthly

-- Usage:
-- spark-sql \
-- --driver-cores 1 \
-- --master yarn \
-- --conf spark.sql.catalog.aqs=com.datastax.spark.connector.datasource.CassandraCatalog \
-- --conf spark.sql.catalog.aqs.spark.cassandra.connection.host=aqs1010-a.eqiad.wmnet:9042,aqs1011-a.eqiad.wmnet:9042,aqs1012-a.eqiad.wmnet:9042 \
-- --conf spark.sql.catalog.aqs.spark.cassandra.auth.username=aqsloader \
-- --conf spark.sql.catalog.aqs.spark.cassandra.auth.password=cassandra \
-- --conf spark.sql.catalog.aqs.spark.cassandra.output.batch.size.rows=1024 \
-- --jars /srv/deployment/analytics/refinery/artifacts/org/wikimedia/analytics/refinery/refinery-job-0.2.4-shaded.jar \
-- --conf spark.dynamicAllocation.maxExecutors=128 \
-- --conf spark.yarn.maxAppAttempts=1 \
-- --conf spark.executor.memoryOverhead=3072  \
-- --executor-memory 8G \
-- --executor-cores 2 \
-- --driver-memory 4G \
--     -f load_cassandra_knowledge_gaps_by_category_file_monthly.hql \
--     -d destination_table=aqs.local_group_default_knowledge_gaps_content_gap_metrics.data \
--     -d source_table=knowledge_gaps.content_gap_metrics \
--     -d date="2021-09"

WITH metrics_with_totals AS (
    SELECT
        REGEXP_REPLACE(cw.domain_name, '.org', '') AS project,
        CONCAT(REGEXP_REPLACE(kgm.time_bucket, '-', ''), '01') AS dt,
        kgm.category,
        kgm.content_gap,
        -- we use the fancy line below to unravel the struct object create a key value mapping to metric
        -- and value fields in the resulting query return
        EXPLODE(FROM_JSON(TO_JSON(kgm.by_category), 'map<string, string>')) AS (metric, value)
    FROM
        ${source_table} AS kgm
    INNER JOIN
        canonical_data.wikis AS cw
        ON (kgm.wiki_db = cw.database_code)
    WHERE
        kgm.time_bucket = '${date}'

    UNION ALL

    SELECT DISTINCT
        REGEXP_REPLACE(cw.domain_name, '.org', '') AS project,
        CONCAT(REGEXP_REPLACE(kgm.time_bucket, '-', ''), '01') AS dt,
        'all-categories' AS category,
        kgm.content_gap,
        EXPLODE(FROM_JSON(TO_JSON(kgm.totals), 'map<string, string>')) AS (metric, value)
    FROM
        ${source_table} AS kgm
    INNER JOIN
        canonical_data.wikis AS cw
        ON (kgm.wiki_db = cw.database_code)
    WHERE
        kgm.time_bucket = '${date}'
)


INSERT INTO ${destination_table}
SELECT
/*+ COALESCE(${coalesce_partitions}) */
    'analytics.wikimedia.org' AS _domain,
    project,
    category,
    content_gap,
    dt,
    '13814000-1dd2-11b2-8080-808080808080' AS _tid,
    metric,
    value
FROM
    metrics_with_totals
