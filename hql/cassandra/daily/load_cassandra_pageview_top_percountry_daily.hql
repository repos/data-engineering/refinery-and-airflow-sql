-- Load pageview top-per-country daily to Cassandra

-- Parameters:
--     destination_table                   -- Cassandra table to write query output.
--     source_table                        -- Fully qualified hive table to compute from.
--     country_deny_list_table             -- Fully qualified table name containing the countries that should be
--                                            excluded from the results
--     disallowed_cassandra_articles_table -- Fully qualified hive table containing article titles we don't want to
--                                            appear in the top list (ex: offensive language, DOS attack
--                                            manipulations,...).
--     year                                -- year of partition to compute from.
--     month                               -- month of partition to compute from.
--     day                                 -- day of partition to compute from.
--     coalesce_partitions                 -- number of partitions for destination data.
--
-- Usage:
-- spark-sql \
-- --driver-cores 1 \
-- --master yarn \
-- --conf spark.sql.catalog.aqs=com.datastax.spark.connector.datasource.CassandraCatalog \
-- --conf spark.sql.catalog.aqs.spark.cassandra.connection.host=aqs1010-a.eqiad.wmnet:9042,aqs1011-a.eqiad.wmnet:9042,aqs1012-a.eqiad.wmnet:9042 \
-- --conf spark.sql.catalog.aqs.spark.cassandra.auth.username=aqsloader \
-- --conf spark.sql.catalog.aqs.spark.cassandra.auth.password=cassandra \
-- --conf spark.sql.catalog.aqs.spark.cassandra.output.batch.size.rows=1024 \
-- --jars /srv/deployment/analytics/refinery/artifacts/org/wikimedia/analytics/refinery/refinery-job-0.2.17-shaded.jar  \
-- --conf spark.dynamicAllocation.maxExecutors=64 \
-- --conf spark.yarn.maxAppAttempts=1 \
-- --conf spark.executor.memoryOverhead=2048  \
-- --executor-memory 8G \
-- --executor-cores 2 \
-- --driver-memory 4G \
-- -f load_cassandra_pageview_top_percountry_daily.hql \
-- -d destination_table=aqs.local_group_default_T_top_percountry.data \
-- -d source_table=wmf.pageview_actor \
-- -d country_deny_list_table=canonical_data.countries \
-- -d disallowed_cassandra_articles_table=wmf.disallowed_cassandra_articles \
-- -d coalesce_partitions=6 \
-- -d year=2022 \
-- -d month=07 \
-- -d day=01


WITH base_data AS (
    SELECT /*+ BROADCAST(disallowed_articles_list), BROADCAST(country_deny_list) */
        src.actor_signature,
        src.geocoded_data['country_code'] AS country_code,
        REGEXP_REPLACE(src.access_method, ' ', '-') AS `access`,
        src.pageview_info['project'] AS project,
        REFLECT(
            'org.json.simple.JSONObject', 'escape', REGEXP_REPLACE(src.pageview_info['page_title'], '\t', '')
        ) AS page_title,
        LPAD(src.year, 4, '0') AS `year`,
        LPAD(src.month, 2, '0') AS `month`,
        LPAD(src.day, 2, '0') AS `day`
    FROM ${source_table} AS src
    LEFT ANTI JOIN ${disallowed_cassandra_articles_table} AS disallowed_articles_list
        ON
            src.pageview_info['project'] = disallowed_articles_list.project
            AND LOWER(src.pageview_info['page_title']) = LOWER(disallowed_articles_list.article)
    LEFT ANTI JOIN ${country_deny_list_table} AS country_deny_list
        ON
            country_deny_list.iso_code = src.geocoded_data['country_code']
            AND country_deny_list.is_protected IS TRUE
    WHERE
        src.year = ${year}
        AND src.month = ${month}
        AND src.day = ${day}
        AND src.agent_type = 'user'
        AND src.pageview_info IS NOT NULL
        AND src.geocoded_data IS NOT NULL
        AND src.is_pageview
        AND src.pageview_info['page_title'] != '-'
        AND src.geocoded_data['country_code'] != '--'
),

raw AS (
    SELECT
        country_code,
        project,
        page_title,
        year,
        month,
        day,
        COALESCE(access, 'all-access') AS `access`,
        COUNT(*) AS total_view_count,
        COUNT(DISTINCT actor_signature) AS unique_actor_count
    FROM base_data
    GROUP BY
        country_code,
        access,
        project,
        page_title,
        year,
        month,
        day
        GROUPING SETS (
            (
                country_code,
                access,
                project,
                page_title,
                year,
                month,
                day
            ),
            (
                country_code,
                project,
                page_title,
                year,
                month,
                day
            )
        )
),

ranked AS (
    SELECT
        country_code,
        access,
        project,
        page_title,
        year,
        month,
        day,
        CEIL(total_view_count / 100) * 100 AS views_ceil,
        RANK() OVER (PARTITION BY access, country_code, year, month, day ORDER BY total_view_count DESC) AS `rank`,
        ROW_NUMBER() OVER (PARTITION BY access, country_code, year, month, day ORDER BY total_view_count DESC) AS rn
    FROM raw
    WHERE unique_actor_count > 1000
),

max_rank AS (
    SELECT
        country_code AS max_rank_country_code,
        access AS max_rank_access,
        year AS max_rank_year,
        month AS max_rank_month,
        day AS max_rank_day,
        rank AS max_rank
    FROM ranked
    WHERE rn = 1001
    GROUP BY
        country_code,
        access,
        year,
        month,
        day,
        rank
)

INSERT INTO ${destination_table}
SELECT
/*+ COALESCE(${coalesce_partitions}) */
    'analytics.wikimedia.org' AS _domain,
    country_code AS country,
    access,
    year,
    month,
    day,
    '13814000-1dd2-11b2-8080-808080808080' AS _tid,
    CONCAT(
        '[',
        CONCAT_WS(
            ',',
            COLLECT_LIST(
                CONCAT(
                    '{"article":"',
                    page_title,
                    '","project":"',
                    project,
                    '","views_ceil":',
                    CAST(views_ceil AS STRING),
                    ',"rank":',
                    CAST(rank AS STRING),
                    '}'
                )
            )
        ),
        ']'
    ) AS articles
FROM ranked
LEFT JOIN max_rank
    ON (
        country_code = max_rank_country_code
        AND access = max_rank_access
        AND year = max_rank_year
        AND month = max_rank_month
        AND day = max_rank_day
    )
WHERE
    rank < COALESCE(max_rank, 1001)
GROUP BY
    country_code,
    access,
    year,
    month,
    day;
