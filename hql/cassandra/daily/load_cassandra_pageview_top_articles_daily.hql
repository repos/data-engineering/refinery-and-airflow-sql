-- Load the pageview top_articles daily to cassandra
--
-- Parameters:
--     destination_table                   -- Cassandra table to write query output.
--     source_table                        -- Fully qualified hive table to compute from.
--     disallowed_cassandra_articles_table -- Fully qualified hive table containing article titles we don't want to
--                                            appear in the top list (ex: offensive language, DOS attack
--                                            manipulations,...).
--     year                                -- year of partition to compute from.
--     month                               -- month of partition to compute from.
--     day                                 -- day of partition to compute from.
--     coalesce_partitions                 -- number of partitions for destination data.
--
-- Usage:
-- spark-sql \
-- --driver-cores 1 \
-- --master yarn \
-- --conf spark.sql.catalog.aqs=com.datastax.spark.connector.datasource.CassandraCatalog \
-- --conf spark.sql.catalog.aqs.spark.cassandra.connection.host=aqs1010-a.eqiad.wmnet:9042,aqs1011-a.eqiad.wmnet:9042,aqs1012-a.eqiad.wmnet:9042 \
-- --conf spark.sql.catalog.aqs.spark.cassandra.auth.username=aqsloader \
-- --conf spark.sql.catalog.aqs.spark.cassandra.auth.password=cassandra \
-- --conf spark.sql.catalog.aqs.spark.cassandra.output.batch.size.rows=1024 \
-- --jars /srv/deployment/analytics/refinery/artifacts/org/wikimedia/analytics/refinery/refinery-job-0.2.4-shaded.jar  \
-- --conf spark.dynamicAllocation.maxExecutors=64 \
-- --conf spark.yarn.maxAppAttempts=1 \
-- --conf spark.executor.memoryOverhead=2048 \
-- --executor-memory 8G \
-- --executor-cores 2 \
-- --driver-memory 4G \
-- --name pageview_top_articles_daily \
-- -f load_cassandra_pageview_top_articles_daily.hql \
-- -d destination_table=aqs.local_group_default_T_top_pageviews.data \
-- -d source_table=wmf.pageview_hourly \
-- -d disallowed_cassandra_articles_table=wmf.disallowed_cassandra_articles \
-- -d coalesce_partitions=6 \
-- -d year=2022 \
-- -d month=07 \
-- -d day=01


WITH unranked AS (
    SELECT /*+ BROADCAST(disallowed_list) */
        source.project,
        REFLECT('org.json.simple.JSONObject', 'escape', REGEXP_REPLACE(page_title, '\t', '')) AS page_title,
        COALESCE(REGEXP_REPLACE(access_method, ' ', '-'), 'all-access') AS `access`,
        LPAD(year, 4, '0') AS `year`,
        LPAD(month, 2, '0') AS `month`,
        LPAD(day, 2, '0') AS `day`,
        SUM(view_count) AS `views`
    FROM ${src_table} AS src
    LEFT OUTER JOIN ${disallowed_cassandra_articles_table} AS disallowed_list
        ON
            src.project = disallowed_list.project
            AND LOWER(page_title) = LOWER(disallowed_list.article)
    WHERE
        year = ${year}
        AND month = ${month}
        AND day = ${day}
        AND agent_type = 'user'
        AND page_title != '-'
        AND disallowed_list.article IS NULL
    GROUP BY
        src.project, REGEXP_REPLACE(page_title, '\t', ''), access_method, year, month, day
        GROUPING SETS (
            (src.project, REGEXP_REPLACE(page_title, '\t', ''), access_method, year, month, day),
            (src.project, REGEXP_REPLACE(page_title, '\t', ''), year, month, day)
        )

),

ranked AS (
    SELECT
        project,
        page_title,
        access,
        year,
        month,
        day,
        views,
        RANK() OVER (PARTITION BY project, access, year, month, day ORDER BY views DESC) AS `rank`,
        ROW_NUMBER() OVER (PARTITION BY project, access, year, month, day ORDER BY views DESC) AS rn
    FROM unranked

),

max_rank AS (
    SELECT
        project AS max_rank_project,
        access AS max_rank_access,
        year AS max_rank_year,
        month AS max_rank_month,
        day AS max_rank_day,
        rank AS max_rank
    FROM ranked
    WHERE
        rn = 1001
    GROUP BY
        project,
        access,
        year,
        month,
        day,
        rank
)

INSERT INTO ${destination_table}
SELECT
/*+ COALESCE(${coalesce_partitions}) */
    'analytics.wikimedia.org' AS _domain,
    project,
    access,
    year,
    month,
    day,
    '13814000-1dd2-11b2-8080-808080808080' AS _tid,
    CONCAT(
        '[',
        CONCAT_WS(
            ',', COLLECT_LIST(
                CONCAT(
                    '{"article":"', page_title,
                    '","views":', CAST(views AS STRING),
                    ',"rank":', CAST(rank AS STRING), '}'
                )
            )
        ), ']'
    ) AS articlesjson
FROM ranked
LEFT JOIN max_rank
    ON (
        project = max_rank_project
        AND access = max_rank_access
        AND year = max_rank_year
        AND month = max_rank_month
        AND day = max_rank_day
    )
WHERE rank < COALESCE(max_rank, 1001)
GROUP BY
    project,
    access,
    year,
    month,
    day
