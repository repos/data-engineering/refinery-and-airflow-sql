WITH page_ids_with_categories AS (
    SELECT
        file_page_id,
        path[path_length - 1] AS primary_category_page_id,
        IF(path_length > 1, path[1], NULL) AS category_page_id
    FROM (
        SELECT
            file_page_id,
            path,
            SIZE(path) AS path_length
        FROM (
            SELECT
                page_id AS file_page_id,
                EXPLODE(category_paths) AS path
            FROM milimetric.category_and_media
            WHERE page_type = 'file'
        )
    )
),

mediawiki_page AS (
    SELECT *
    FROM wmf_raw.mediawiki_page
    WHERE
        wiki_db = 'commonswiki'
        AND snapshot = '2023-10'
),

mediawiki_revision AS (
    SELECT *
    FROM wmf_raw.mediawiki_revision
    WHERE
        wiki_db = 'commonswiki'
        AND snapshot = '2023-10'
),

mediawiki_actor AS (
    SELECT *
    FROM wmf_raw.mediawiki_private_actor
    WHERE
        wiki_db = 'commonswiki'
        AND snapshot = '2023-10'
)

SELECT
    pids_and_page_titles_and_edit_info.file_page_id,
    pids_and_page_titles_and_edit_info.primary_category_page_ids,
    pids_and_page_titles_and_edit_info.category_page_ids,
    pids_and_page_titles_and_edit_info.file_page_title,
    pids_and_page_titles_and_edit_info.primary_category_page_titles,
    pids_and_page_titles_and_edit_info.category_page_titles,
    pids_and_page_titles_and_edit_info.edit_timestamp,
    pids_and_page_titles_and_edit_info.edit_type,
    IF(is_edit_actor_visible AND mwa.actor_user IS NOT NULL, mwa.actor_name, 'anonymous') AS edit_user
FROM (
    SELECT
        pids_and_page_titles.*,
        mwr.rev_actor AS edit_actor,
        mwr.rev_deleted & 2 = 0 AS is_edit_actor_visible,
        TO_TIMESTAMP(mwr.rev_timestamp, 'yyyyMMddkkmmss') AS edit_timestamp,
        IF(mwr.rev_parent_id == 0, 'create', 'update') AS edit_type -- we want delete as well but need to figure
    -- how to get that one separately
    FROM (
        SELECT
            file_page_id,
            COLLECT_SET(primary_category_page_id) AS primary_category_page_ids,
            COLLECT_SET(category_page_id) AS category_page_ids,
            FIRST(file_page_title) AS file_page_title,
            COLLECT_SET(primary_category_page_title) AS primary_category_page_titles,
            COLLECT_SET(category_page_title) AS category_page_titles
        FROM (
            SELECT
                pids.*,
                mwp1.page_title AS file_page_title,
                mwp2.page_title AS primary_category_page_title,
                mwp3.page_title AS category_page_title
            FROM page_ids_with_categories AS pids
            INNER JOIN mediawiki_page AS mwp1 ON pids.file_page_id = mwp1.page_id
            INNER JOIN mediawiki_page AS mwp2 ON pids.primary_category_page_id = mwp2.page_id
            INNER JOIN mediawiki_page AS mwp3 ON pids.category_page_id = mwp3.page_id
        )
        GROUP BY file_page_id
    ) AS pids_and_page_titles
    INNER JOIN mediawiki_revision AS mwr ON pids_and_page_titles.file_page_id = mwr.rev_page
) AS pids_and_page_titles_and_edit_info
INNER JOIN mediawiki_actor AS mwa ON edit_actor = mwa.actor_id
